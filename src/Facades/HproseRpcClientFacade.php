<?php

namespace kingdu\laravel\plugins\Facades;

use \Illuminate\Support\Facades\Facade;

class HproseRpcClientFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'rpc.client';
    }
}
