<?php

return [
    'server'=>[
        /* type = [hprose, swoole] */
        'type' => 'hprose',
        'uri' => 'tcp://0.0.0.0:1314'
    ],

    'services'=>[
        'Model\User' => 'User',
    ],

    'client'=>[
        /* type = [hprose, swoole] */
        'type' => 'hprose',
        'uri' => 'tcp://127.0.0.1:1314',
        'class' =>[
            'namespace' => 'App\Rpc\Client',
            'path' => app_path('Rpc/Client')
        ]
     ],
];
