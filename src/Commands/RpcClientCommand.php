<?php

namespace kingdu\laravel\plugins\Commands;

use Illuminate\Console\Command;

class RpcClientCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rpc:generator';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create client class code.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = app('client.generator');
        $client->generator();
    }
}
