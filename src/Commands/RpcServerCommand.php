<?php

namespace kingdu\laravel\plugins\Commands;

use Illuminate\Console\Command;

class RpcServerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rpc:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'start rpc server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->start();
    }

    /**
     *
     */
    public function start()
    {
        $server = app('rpc.server');
        $this->load($server);

        print "\nServer running at:\n";
        print "- Local: ".config('rpc')['server']['uri']."\n\n";
        print "String services name:\n";
        print_r($server->getNames());
        print "\n";

        $server->start();
    }

    /**
     * @param $server
     */
    public function load($server)
    {
        $services = $this->get_services();
        $keys = array_keys($services);

        foreach( $keys as $key )
        {
            $value = $services[$key];
            $server->addInstanceMethods(new $key, null, $value);
        }
    }

    /**
     * loader services
     * @param $config
     * @return array
     */
    private function get_services():array
    {
        return config('rpc')['services'];
    }
}
