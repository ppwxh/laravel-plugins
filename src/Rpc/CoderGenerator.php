<?php


namespace kingdu\laravel\plugins\Rpc;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

use ReflectionClass;

class CoderGenerator
{
    /**
     * @param string $ns
     */
    public function generator()
    {
        $loader = new FilesystemLoader();
        try {
            $loader->addPath(config_path());
        } catch (LoaderError $e) {
            print_r($e->getTrace());
        }

        $twig = new Environment($loader);
        $this->render_client_class($twig);
    }

    /**
     * @param $twig
     * @param $ns
     */
    private function render_client_class($twig)
    {
        $class_cfg = config('rpc')['client']['class'];
        $class_space = $class_cfg['namespace'];
        $class_path = $class_cfg['path'];

        $services = $this->get_services();
        $class_names = array_keys($services);
        foreach ($class_names as $class_name)
        {
            $alias_name = $services[$class_name];
            try {
                $data = $twig->render('client.blade.twig', $this->get_method_info($class_name, $alias_name, $class_space));
                $this->save_to_file($class_path, $alias_name, $data);
            } catch (LoaderError | RuntimeError | SyntaxError $e) {
                print_r($e->getTrace());
            }
        }

    }

    /**
     * @param $class_name
     * @param $alias_name
     * @param $class_space
     * @return array
     */
    private function get_method_info($class_name, $alias_name, $class_space): array
    {
        $func_array = [];
        try {
            $class = new ReflectionClass($class_name);
            $methods = $class->getMethods();
            foreach ($methods as $method)
            {
                /**
                 * 过滤一些特殊的函数
                 */
                if($method->isPublic() &&
                    !$method->isStatic() &&
                    !$method->isConstructor() &&
                    !$method->isDestructor() &&
                    !$method->isAbstract()) {

                    $mn = $method->getName();
                    $func_params = $this->get_method_parameters($method->getParameters());

                    /**
                     * 返回值处理
                     */
                    $rt = $method->getReturnType();
                    if(isset($rt)) {
                        array_push($func_array, ['funcName' => $mn.$func_params, 'retType' =>': '.$rt]);
                    }
                    else {
                        array_push($func_array, ['funcName' => $mn.$func_params]);
                    }
                }
            }
        } catch (\ReflectionException $e) {
            print_r($e->getTrace());
        }

        return [
            'ns' => $class_space,
            'className' => $alias_name,
            'methods' => $func_array
        ];
    }

    /**
     * @param $class_path
     * @param $file_name
     * @param $data
     */
    private function save_to_file($class_path, $file_name, $data)
    {
        /*检查目标目录是否存在*/
        if(!file_exists($class_path))
        {
            //创建目录
            mkdir($class_path,0777, true);
        }

        /**
         * save file data.
         */
        $file_name =$class_path.'/'.$file_name.'.php';
        $file_num = fopen($file_name,'w');
        if(!$file_num)
        {
            print $file_name." open file error!";
            return;
        }

        flock($file_num,LOCK_EX);
        fwrite($file_num, $data);
        flock($file_num,LOCK_UN);
        fclose($file_num);

        print "\n\n".$file_name."php file generated successfully.\n\n";
    }

    /**
     * loader services
     * @param $config
     * @return array
     */
    private function get_services():array
    {
        return config('rpc')['services'];
    }

    /**
     * @param $parameters
     * @return string
     */
    private function get_method_parameters($parameters): string
    {
        $func_params = '(';

        foreach ($parameters as $parameter) {
            $func_params = $func_params.'$'.$parameter->getName();
            if($parameter != end($parameters))
                $func_params = $func_params.', ';
        }

        $func_params = $func_params.')';

        return $func_params;
    }

}
