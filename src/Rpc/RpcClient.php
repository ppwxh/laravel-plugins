<?php


namespace kingdu\laravel\plugins\Rpc;

use Hprose\Socket\Client as HproseClient;
use Hprose\Swoole\Socket\Client as SwooleClient;

class RpcClient
{
    private function __construct(){}

    /**
     * creat rpc client object
     * @return HproseClient|SwooleClient|null
     */
    static public function create(): HproseClient|SwooleClient|null
    {
        $cfg = config('rpc');
        $uri = $cfg["client"]["uri"];
        if(in_array("hprose", $cfg["server"]))
        {
            return new HproseClient($uri, false);
        }
        else if(in_array("swoole", $cfg["server"]))
        {
            return new SwooleClient($uri, false);
        }
        else
        {
            print "The type not supported !\n";
            return null;
        }
    }
}
