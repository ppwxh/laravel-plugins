<?php


namespace kingdu\laravel\plugins\Rpc;

use Hprose\Socket\Server as HposerServer;
use Hprose\Swoole\Socket\Server as SwooleServer;
class RpcServer
{
    private function __construct(){}

    /**
     * @param $config
     * @return SwooleServer|RpcServer|HposerServer|null
     */
    static public function create(): SwooleServer|RpcServer|HposerServer|null
    {
        $cfg = config('rpc');
        $uri = $cfg["server"]["uri"];

        if(in_array("hprose", $cfg["server"]))
        {
            return new HposerServer($uri);
        }
        else if(in_array("swoole", $cfg["server"]))
        {
            return new SwooleServer($uri);
        }
        else
        {
            print "The type not supported !\n";
            return null;
        }
    }
}
