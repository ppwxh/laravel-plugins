<?php

namespace kingdu\laravel\plugins;

use Illuminate\Support\ServiceProvider;
use kingdu\laravel\plugins\Rpc\RpcClient;
use kingdu\laravel\plugins\Rpc\RpcServer;
use kingdu\laravel\plugins\Rpc\CoderGenerator;

class RpcServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('rpc.server', function($app){
            return RpcServer::create();
        });

        $this->app->bind('rpc.client', function($app){
            return RpcClient::create();
        });

        $this->app->singleton('client.generator', function($app){
            return new CoderGenerator();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * copy /Config/rpc.php to config/rpc.php
         */

        $this->publishes([
            __DIR__.'/Config/rpc.php' => config_path('rpc.php'),
            __DIR__.'/Config/client.blade.twig' => config_path('client.blade.twig')
        ], 'rpc' /* --tag=public*/ );

        $this->commands(array(Commands\RpcServerCommand::class, Commands\RpcClientCommand::class));
   }

}
