# Hprose Rpc for laravel8.0 plugin

#### 介绍
Hprose Rpc for PHP 整合laravel8.0插件，通过这个插件方便的进行RPC程序的开发；

1、自动加载服务端程序；

2、自动生成客户端程序；

3、支持Socket、swoole

#### 软件架构
软件架构说明


#### 安装教程

1.  composer require "kingdu/laravel-plugins": "dev-master"
2.  安装配置文件
    php artisan vendor:publish --tag=rpc
3.  修改配置文件


    <?php

return [

    /**
    *   服务端配置
    */
    
    'server'=>[
    /* type = [hprose, swoole] */
    'type' => 'hprose',
    'uri' => 'tcp://0.0.0.0:1314'
    ],
    
    /**
     *  服务端程序的命名空间及类名，用于服务端自动加载
     */
    
    'services'=>[
        'Model\User' => 'User',
    ],
    
    /**
     *  客户端配置
     */
    
    'client'=>[
        /* type = [hprose, swoole] */
        'type' => 'hprose',
        'uri' => 'tcp://127.0.0.1:1314',
         /*生成客户端代理类的路径及命名空间*/
        'class' =>[
            'path' => app_path('Rpc/Client'),
            'namespace' => 'App\Rpc\Client',
        ]
     ],
];


#### 使用说明

1.  编写服务端程序
    
    App\Service

    class User{

          public function showUser():string
          {
            return "user";
          }
    }

2.  生成客户端代码
    php artisan rpc:generator

3.  运行服务端
    php artisan rpc:start

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
